package com.cppba.alipay.util;

import com.cppba.alipay.base.exception.AlipayException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

/**
 * @author winfed
 * @create 2017-10-18 17:19
 */
@Slf4j
public class AlipayJsonUtils {

    // 利用静态内部类特性实现外部类的单例
    private static class ObjectMapperBuilder{
        private static final  ObjectMapper mapper;
        static {
            mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
    }

    private static ObjectMapper getMapper() {
        return ObjectMapperBuilder.mapper;
    }

    public static <T> T jsonToBean(String jsonStr, Class<T> cls) throws AlipayException {
        try {
            return getMapper().readValue(jsonStr, cls);
        } catch (IOException e) {
            throw new AlipayException(e.getMessage());
        }
    }

    public static <T> T mapToBean(Map map, Class<T> cls) throws AlipayException {
        try {
            String json = getMapper().writeValueAsString(map);
            return getMapper().readValue(json, cls);
        } catch (IOException e) {
            throw new AlipayException(e.getMessage());
        }
    }

    public static String beanToJson(Object src) throws AlipayException {
        try {
            return getMapper().writeValueAsString(src);
        } catch (JsonProcessingException e) {
            throw new AlipayException(e.getMessage());
        }
    }
}
