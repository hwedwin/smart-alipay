package com.cppba.alipay.base.domain.constant;

/**
 * 获取支付宝接口地址
 *
 * @author winfed
 * @create 2017-10-18 16:50
 */
public class AlipayBaseUrl {
    // 正式测试环境
    private final static String httpsReqUrl = "https://openapi.alipay.com/gateway.do";

    // 沙箱测试环境账号
    private final static String devReqUrl = "https://openapi.alipaydev.com/gateway.do";

    /**
     * 获取接口地址
     *
     * @param isSandbox 是否是沙箱模式
     * @return
     */
    public static String getBaseUrl(Boolean isSandbox) {
        if (isSandbox) {
            return devReqUrl;
        }
        return httpsReqUrl;
    }
}
