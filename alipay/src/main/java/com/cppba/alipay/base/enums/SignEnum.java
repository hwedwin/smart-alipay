package com.cppba.alipay.base.enums;

import com.cppba.alipay.util.SignUtils;

import java.util.Map;

/**
 * @author winfed
 * @create 2017-11-07 10:42
 */
public enum SignEnum {

    /**
     * 支付宝暂时用不到
     */
    @Deprecated
    MD5 {
        /**
         * @param content 需要签名的内容
         * @param key 密钥
         * @return 签名值
         */
        @Override
        public String createSign(String content, String key, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.MD5.sign(content, key, characterEncoding);
        }

        /**
         * 签名字符串
         *
         * @param text 需要签名的字符串
         * @param sign 签名结果
         * @param key 密钥
         * @return 签名结果
         */
        public boolean verify(String text, String sign, String key, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.MD5.verify(text, sign, key, characterEncoding);
        }
    },
    RSA {
        @Override
        public String createSign(String content, String privateKey, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.RSA.sign(content, privateKey, characterEncoding);
        }

        @Override
        public boolean verify(String text, String sign, String publicKey, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.RSA.verify(text, sign, publicKey, characterEncoding);
        }
    },
    RSA2 {
        @Override
        public String createSign(String content, String key, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.RSA2.sign(content, key, characterEncoding);
        }

        @Override
        public boolean verify(String text, String sign, String publicKey, String characterEncoding) {
            return com.cppba.alipay.util.sign.encrypt.RSA2.verify(text, sign, publicKey, characterEncoding);
        }
    };

    /**
     * 签名
     *
     * @param content           需要签名的内容
     * @param privateKey        密钥
     * @param characterEncoding 字符编码
     * @return 签名值
     */
    public abstract String createSign(String content, String privateKey, String characterEncoding);

    /**
     * 签名字符串
     *
     * @param params    需要签名的字符串
     * @param sign      签名结果
     * @param publicKey 密钥
     * @return 签名结果
     */
    public boolean verify(Map params, String sign, String publicKey) {
        // 判断是否一样
        return this.verify(SignUtils.parameterText(params), sign, publicKey, "utf-8");
    }

    /**
     * 签名字符串
     *
     * @param params            需要签名的字符串
     * @param sign              签名结果
     * @param publicKey         密钥
     * @param characterEncoding 编码格式
     * @return 签名结果
     */
    public boolean verify(Map params, String sign, String publicKey, String characterEncoding) {
        // 判断是否一样
        return this.verify(SignUtils.parameterText(params), sign, publicKey, characterEncoding);
    }

    /**
     * 签名字符串
     *
     * @param text      需要签名的字符串
     * @param sign      签名结果
     * @param publicKey 密钥
     * @return 签名结果
     */
    public boolean verify(String text, String sign, String publicKey) {
        return this.verify(text, sign, publicKey, "utf-8");
    }

    /**
     * 签名字符串
     *
     * @param text              需要签名的字符串
     * @param sign              签名结果
     * @param publicKey         密钥
     * @param characterEncoding 编码格式
     * @return 签名结果
     */
    public abstract boolean verify(String text, String sign, String publicKey, String characterEncoding);
}
