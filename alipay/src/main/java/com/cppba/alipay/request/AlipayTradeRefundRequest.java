package com.cppba.alipay.request;


import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.response.AlipayTradeQueryResponse;
import com.cppba.alipay.response.AlipayTradeRefundResponse;

/**
 * 统一收单交易退款接口
 * 接口名称：alipay.trade.refund
 *
 * @author winfed
 * @create 2017-11-21 15:35
 */
public class AlipayTradeRefundRequest extends AlipayRequest<AlipayTradeRefundResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayTradeRefundRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayTradeRefundRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        super(isSandbox, "alipay.trade.refund", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
        super.methodName = "统一收单交易退款接口";
        super.clazz = AlipayTradeRefundResponse.class;
    }

    /**
     * 构建请求返回对象
     *
     * @return
     * @throws AlipayException
     */
    @Override
    public AlipayTradeRefundResponse createResponse() throws AlipayException {
        return sendRequest();
    }
}
