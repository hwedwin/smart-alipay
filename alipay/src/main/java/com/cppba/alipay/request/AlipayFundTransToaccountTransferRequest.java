package com.cppba.alipay.request;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.base.response.AlipayResponse;
import com.cppba.alipay.response.AlipayFundTransToaccountTransferResponse;
import com.cppba.alipay.response.AlipayOpenAuthTokenAppQueryResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 单笔转账到支付宝账户接口
 * 接口名称：alipay.fund.trans.toaccount.transfer
 * url https://docs.open.alipay.com/api_28/alipay.fund.trans.toaccount.transfer
 * @author winfed
 * @create 2018-11-9 16:57
 */
public class AlipayFundTransToaccountTransferRequest extends AlipayRequest<AlipayFundTransToaccountTransferResponse> {
	/**
	 * @param appId            应用ID
	 * @param privateKey       应用私钥
	 * @param alipayPublicKey  支付宝公钥
	 * @param alipayBizContent 商户授权令牌
	 */
	public AlipayFundTransToaccountTransferRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
		this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent);
	}

	/**
	 * @param isSandbox        是否沙箱模式
	 * @param appId            应用ID
	 * @param privateKey       应用私钥
	 * @param alipayPublicKey  支付宝公钥
	 * @param alipayBizContent 商户授权令牌
	 */
	public AlipayFundTransToaccountTransferRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
		super(isSandbox, "alipay.fund.trans.toaccount.transfer", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
		super.methodName = "单笔转账到支付宝账户接口";
		super.clazz = AlipayFundTransToaccountTransferResponse.class;
	}

	/**
	 * 构建请求返回对象
	 *
	 * @return
	 * @throws AlipayException
	 */
	@Override
	public AlipayFundTransToaccountTransferResponse createResponse() throws AlipayException {
		return sendRequest();
	}
}