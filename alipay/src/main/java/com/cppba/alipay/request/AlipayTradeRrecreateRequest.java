package com.cppba.alipay.request;


import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.response.AlipayTradePrecreateResponse;
import org.apache.commons.lang3.StringUtils;

/**
 * 统一收单线下交易预创建
 * 接口名称：alipay.trade.precreate
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
public class AlipayTradeRrecreateRequest extends AlipayRequest<AlipayTradePrecreateResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeRrecreateRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent, String notifyUrl, String appAuthToken) {
        this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent, notifyUrl, appAuthToken);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeRrecreateRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent, String notifyUrl, String appAuthToken) {
        /////////////////////////////////////
        super(isSandbox, "alipay.trade.precreate", appId, privateKey, alipayPublicKey, signType, alipayBizContent);

        super.methodName = "统一收单线下交易预创建";
        super.clazz = AlipayTradePrecreateResponse.class;

        // 公共参数
        if (StringUtils.isNotBlank(appAuthToken)) {
            this.addParameter("app_auth_token", appAuthToken);
        }
        if (StringUtils.isNotBlank(notifyUrl)) {
            this.addParameter("notify_url", notifyUrl);
        }
    }

    /**
     * 构建请求返回对象
     *
     * @return
     * @throws AlipayException
     */
    @Override
    public AlipayTradePrecreateResponse createResponse() throws AlipayException {
        return sendRequest();
    }
}
