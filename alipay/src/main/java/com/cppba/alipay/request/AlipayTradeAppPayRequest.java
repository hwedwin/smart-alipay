package com.cppba.alipay.request;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.base.response.AlipayResponse;
import com.cppba.alipay.util.UriVariableUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * App支付(替app拼接参数，不发起实质调用)
 * 接口名称：alipay.trade.app.pay
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
public class AlipayTradeAppPayRequest extends AlipayRequest<AlipayResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeAppPayRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signTye, AlipayBizContent alipayBizContent, String notifyUrl) {
        this(false, appId, privateKey, alipayPublicKey, signTye, alipayBizContent, notifyUrl);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeAppPayRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signTye, AlipayBizContent alipayBizContent, String notifyUrl) {
        /////////////////////////////////////
        super(isSandbox, "alipay.trade.app.pay", appId, privateKey, alipayPublicKey, signTye, alipayBizContent);
        super.methodName = "App支付";
        super.clazz = AlipayResponse.class;
        // 公共参数
        if (StringUtils.isNotBlank(notifyUrl)) {
            this.addParameter("notify_url", notifyUrl);
        }
    }

    /**
     * 返回给移动端可直接调用的参数集合
     *
     * @return
     */
    public String createResposeParameter() {
        Map<String, Object> parameters = this.getPreRequestParameters();
        return UriVariableUtils.getMapToParameters(parameters);
    }
}
