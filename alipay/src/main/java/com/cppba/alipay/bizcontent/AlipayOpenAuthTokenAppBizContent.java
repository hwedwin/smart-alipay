package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayOpenAuthTokenAppBizContent extends AlipayBizContent {
    /**
     * 授权类型
     * authorization_code
     * 如果使用app_auth_code换取token，则为authorization_code，如果使用refresh_token换取新的token，则为refresh_token
     */
    @JsonProperty("grant_type")
    private String grantType;

    /**
     * 授权码
     * bf67d8d5ed754af297f72cc482287X62
     * 与refresh_token二选一，用户对应用授权后得到，即第一步中开发者获取到的app_auth_code值
     */
    @JsonProperty("code")
    private String code;

    /**
     * 刷新令牌
     * 201510BB0c409dd5758b4d939d4008a525463X62
     * 与code二选一，可为空，刷新令牌时使用
     */
    @JsonProperty("refresh_token")
    private String refreshToken;
}
