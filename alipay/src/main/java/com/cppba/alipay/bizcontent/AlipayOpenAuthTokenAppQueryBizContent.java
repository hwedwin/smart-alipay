package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayOpenAuthTokenAppQueryBizContent extends AlipayBizContent {
    /**
     * 商户授权令牌
     * 201510BBaabdb44d8fd04607abf8d5931ec75D84
     * 通过该令牌来帮助商户发起请求，完成业务
     */
    @JsonProperty("app_auth_token")
    private String appAuthtoken;
}
