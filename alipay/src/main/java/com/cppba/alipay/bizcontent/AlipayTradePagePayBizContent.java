package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradePagePayBizContent extends AlipayBizContent {
    /**
     * 必填
     * 商户订单号，64个字符以内、可包含字母、数字、下划线；需保证在商户端不重复
     * 20150320010101001
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 必填
     * 销售产品码，与支付宝签约的产品码名称。 注：目前仅支持FAST_INSTANT_TRADE_PAY
     * FAST_INSTANT_TRADE_PAY
     */
    @JsonProperty("product_code")
    private String productCode = "FAST_INSTANT_TRADE_PAY";

    /**
     * 必填
     * 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
     * 88.88
     */
    @JsonProperty("total_amount")
    private String totalAmount;

    /**
     * 必填
     * 订单标题
     * Iphone6 16G
     */
    @JsonProperty("subject")
    private String subject;

    /**
     * 订单描述
     * Iphone6 16G
     */
    @JsonProperty("body")
    private String body;

    /**
     * 订单包含的商品列表信息，Json格式： {"show_url":"https://或http://打头的商品的展示地址"} ，在支付时，可点击商品名称跳转到该地址
     * {"show_url":"https://www.alipay.com"}
     */
    @JsonProperty("goods_detail")
    private String goodsDetail;

    /**
     * 公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。支付宝只会在异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给支付宝
     * merchantBizType%3d3C%26merchantBizNo%3d2016010101111
     */
    @JsonProperty("passback_params")
    private String passbackParams;

    /**
     * 业务扩展参数
     * {"sys_service_provider_id":"2088511833207846"}
     */
    @JsonProperty("extend_params")
    private ExtendParams extendParams;

    /**
     * 商品主类型：0—虚拟类商品，1—实物类商品（默认）
     * 注：虚拟类商品不支持使用花呗渠道
     * 0
     */
    @JsonProperty("goods_type")
    private String goodsType;

    /**
     * （1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。该参数在请求到支付宝时开始计时。
     * 2h
     */
    @JsonProperty("timeout_express")
    private String timeoutExpress;

    /**
     * 可用渠道，用户只能在指定渠道范围内支付
     * 当有多个渠道时用“,”分隔
     * 注：与disable_pay_channels互斥
     * pcredit,moneyFund,debitCardExpress
     */
    @JsonProperty("enable_pay_channels")
    private String enablePayChannels;

    /**
     * 禁用渠道，用户不可用指定渠道支付
     * 当有多个渠道时用“,”分隔
     * 注：与enable_pay_channels互斥
     * Iphone6 16G
     */
    @JsonProperty("disable_pay_channels")
    private String disablePayChannels;

    /**
     * 获取用户授权信息，可实现如免登功能。获取方法请查阅：用户信息授权
     * appopenBb64d181d0146481ab6a762c00714cC27
     */
    @JsonProperty("auth_token")
    private String authToken;

    /**
     * PC扫码支付的方式，支持前置模式和跳转模式。
     * 前置模式是将二维码前置到商户的订单确认页的模式。需要商户在自己的页面中以iframe方式请求支付宝页面。具体分为以下几种：
     * 0：订单码-简约前置模式，对应iframe宽度不能小于600px，高度不能小于300px；
     * 1：订单码-前置模式，对应iframe宽度不能小于300px，高度不能小于600px；
     * 3：订单码-迷你前置模式，对应iframe宽度不能小于75px，高度不能小于75px；
     * 4：订单码-可定义宽度的嵌入式二维码，商户可根据需要设定二维码的大小。
     * 跳转模式下，用户的扫码界面是由支付宝生成的，不在商户的域名下。
     * 2：订单码-跳转模式
     * 4
     */
    @JsonProperty("qr_pay_mode")
    private String qrPayMode;

    /**
     * 商户自定义二维码宽度
     * 注：qr_pay_mode=4时该参数生效
     * 100
     */
    @JsonProperty("qrcode_width")
    private String qrcodeWidth;

    /**
     * 业务扩展参数
     */
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ExtendParams {
        /**
         * 系统商编号，该参数作为系统商返佣数据提取的依据，请填写系统商签约协议的PID
         * 2088511833207846
         */
        @JsonProperty("sys_service_provider_id")
        private String sysWerviceProviderId;

        /**
         * 花呗分期数（目前仅支持3、6、12）
         * 注：使用该参数需要仔细阅读“花呗分期接入文档”
         * 3
         */
        @JsonProperty("hb_fq_num")
        private String hbFqNum;

        /**
         * 卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持
         * 注：使用该参数需要仔细阅读“花呗分期接入文档”
         * 100
         */
        @JsonProperty("hb_fq_seller_percent")
        private String hbFqSellerPercent;
    }

}
