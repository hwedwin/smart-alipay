package com.cppba.alipay.properties;

import com.cppba.alipay.base.enums.SignEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author winfed
 * @create 2017-10-23 15:35
 */
@Data
@Component
@ConfigurationProperties(prefix = "alipay")
@PropertySource(value = "smart-alipay.properties")
public class AlipayProperties {
    /**
     * 是否沙箱环境
     */
    private Boolean isTest;
    /**
     * 应用ID
     */
    private String appId;
    /**
     * 应用私钥
     */
    private String privateKey;
    /**
     * 支付宝公钥
     */
    private String alipayPublicKey;
    /**
     * 签名方式
     * RSA,RSA2
     */
    private SignEnum signType;
    /**
     * 支付异步回调地址
     */
    private String notifyUrl;
    /**
     * 订单过期时间
     */
    private String timeoutExpress;
}
