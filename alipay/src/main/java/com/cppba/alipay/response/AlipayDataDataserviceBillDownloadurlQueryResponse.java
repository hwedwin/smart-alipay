package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 查询对账单下载地址
 * 接口名称：alipay.data.dataservice.bill.downloadurl.query
 * url https://docs.open.alipay.com/api_15/alipay.data.dataservice.bill.downloadurl.query
 * @author winfed
 * @create 2018-11-10 16:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayDataDataserviceBillDownloadurlQueryResponse extends AlipayResponse {

	@JsonProperty("bill_download_url")
	private String billDownloadUrl;

}