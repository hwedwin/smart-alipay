package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 查询授权信息
 * 接口名称：alipay.open.auth.token.app.query
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayOpenAuthTokenAppQueryResponse extends AlipayResponse {

    /**
     * 授权者的PID
     * 2088011177545623
     */
    @JsonProperty("user_id")
    private String userId;

    /**
     * 授权商户的AppId（如果有服务窗，则为服务窗的AppId）
     * 2013111800001989
     */
    @JsonProperty("auth_app_id")
    private String authAppId;

    /**
     * 交换令牌的有效期，单位秒，换算成天的话为365天
     * 31536000
     */
    @JsonProperty("expires_in")
    private Long expiresIn;

    /**
     * 授权接口列表
     * "alipay.open.auth.token.app.query","alipay.system.oauth.token","alipay.open.auth.token.app"
     */
    @JsonProperty("auth_methods")
    private List<String> authMethods;

    /**
     * 授权生效时间
     * 2015-11-03 01:59:57
     */
    @JsonProperty("auth_start")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date authStart;

    /**
     * 授权失效时间
     * 2016-11-03 01:59:57
     */
    @JsonProperty("auth_end")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date authEnd;

    /**
     * 状态
     * valid
     */
    @JsonProperty("status")
    private String status;

}
