package com.cppba.alipay.controller;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.bizcontent.AlipayFundTransToaccountTransferBizContent;
import com.cppba.alipay.bizcontent.AlipayTradeAppPayBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayFundTransToaccountTransferRequest;
import com.cppba.alipay.request.AlipayTradeAppPayRequest;
import com.cppba.alipay.response.AlipayFundTransToaccountTransferResponse;
import com.cppba.alipay.util.MoneyUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.Random;

/**
 * 单笔转账到支付宝账户接口
 *
 * @author winfed
 * @create 2018-11-9 16:57
 */
@Slf4j
@Controller
@RequestMapping("/alipay")
public class AlipayFundTransToaccountTransferController extends AlipayBizContent {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayFundTransToaccountTransferController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 拼接app支付需要参数
     *
     * @return app支付参数
     */
    @RequestMapping(value = "/fund/trans/toaccount/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public AlipayFundTransToaccountTransferResponse alipayFundTransToaccountTransferController() {
        try {
            // http://127.0.0.1:8080/alipay/fund/trans/toaccount/transfer
            // 模拟数据
            String outBizNo =  "No" + new Random(new Date().getTime()).nextInt();
            String payeeType = "ALIPAY_LOGONID";
            String payeeAccount = "18808769536";
            String amount = "100";
            String remark = "test";
            // 业务参数
            AlipayFundTransToaccountTransferBizContent alipayFundTransToaccountTransferBizContent = new AlipayFundTransToaccountTransferBizContent();
            alipayFundTransToaccountTransferBizContent.setOutBizNo(outBizNo);
            alipayFundTransToaccountTransferBizContent.setPayeeType(payeeType);
            alipayFundTransToaccountTransferBizContent.setPayeeAccount(payeeAccount);
            alipayFundTransToaccountTransferBizContent.setAmount(amount);
            alipayFundTransToaccountTransferBizContent.setRemark(remark);
            // 请求构建对象
            AlipayFundTransToaccountTransferRequest alipayFundTransToaccountTransferRequest = new AlipayFundTransToaccountTransferRequest(alipayProperties.getIsTest(), alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                    alipayProperties.getSignType(), alipayFundTransToaccountTransferBizContent);
            // 发送请求，获取调用结果
            AlipayFundTransToaccountTransferResponse response = alipayFundTransToaccountTransferRequest.createResponse();
            // 打印返回结果
            log.info(response.toString());
            return response;
        } catch (AlipayException e) {
            log.error("", e);
        }
        return null;
    }
}