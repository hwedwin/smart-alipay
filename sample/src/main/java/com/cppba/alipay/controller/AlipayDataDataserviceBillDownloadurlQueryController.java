package com.cppba.alipay.controller;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.bizcontent.AlipayDataDataserviceBillDownloadurlQueryBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayDataDataserviceBillDownloadurlQueryRequest;
import com.cppba.alipay.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 查询对账单下载地址
 *
 * @author winfed
 * @create 2018-11-10 16:57
 */
@Slf4j
@Controller
@RequestMapping("/alipay")
public class AlipayDataDataserviceBillDownloadurlQueryController extends AlipayBizContent {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayDataDataserviceBillDownloadurlQueryController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 拼接app支付需要参数
     *
     * @return app支付参数
     */
    @RequestMapping(value = "/data/dataservice/bill/downloadurl/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public AlipayDataDataserviceBillDownloadurlQueryResponse alipayFundTransToaccountTransferController() {
        try {
            // http://127.0.0.1:8080/alipay/data/dataservice/bill/downloadurl/query
            // 模拟数据
            // signcustomer,trade
            String billType =  "signcustomer";
            String billDate = "2018-11-09";

            // 业务参数
            AlipayDataDataserviceBillDownloadurlQueryBizContent alipayDataDataserviceBillDownloadurlQueryBizContent = new AlipayDataDataserviceBillDownloadurlQueryBizContent();
            alipayDataDataserviceBillDownloadurlQueryBizContent.setBillType(billType);
            alipayDataDataserviceBillDownloadurlQueryBizContent.setBillDate(billDate);
            // 请求构建对象
            AlipayDataDataserviceBillDownloadurlQueryRequest alipayDataDataserviceBillDownloadurlQueryRequest = new AlipayDataDataserviceBillDownloadurlQueryRequest(alipayProperties.getIsTest(), alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                    alipayProperties.getSignType(), alipayDataDataserviceBillDownloadurlQueryBizContent);
            // 发送请求，获取调用结果
            AlipayDataDataserviceBillDownloadurlQueryResponse response = alipayDataDataserviceBillDownloadurlQueryRequest.createResponse();
            // 打印返回结果
            log.info(response.toString());
            return response;
        } catch (AlipayException e) {
            log.error("", e);
        }
        return null;
    }
}